#include <string.h>
#include <stdlib.h>
#include "mstate.h"
#include "mstring.h"
#include "mutil.h"

#define HASH_LIMIT 5

#define mod(x,y) ((x)&((y)-1))

static int str_hash_value(const char* str, int len)
{
  int h = 0;
  int l;
  int step = (len >> HASH_LIMIT) + 1;
  for (l = len; l >= step; l -= step) {
    h = h ^ ((h << 5) + (h >> 2) + str[l-1]);
  }

  return h;
}

static void resize_strtable(struct strtable* strtab)
{
  int i;
  MString** obj;
  MString* tmp;

  strtab->table = (MString**)realloc(strtab->table, sizeof(MString*)* 2*strtab->size);
  memset(strtab->table + strtab->size, 0, sizeof(MString*) * strtab->size);

  for (i = 0; i < strtab->size; ++i) {
    obj = &strtab->table[i];
    while (*obj != NULL) {
      if ((*obj)->hash & strtab->size) {
	tmp = (*obj)->next;
	(*obj)->next = strtab->table[i+strtab->size];
	strtab->table[i+strtab->size] = (*obj);
	(*obj) = tmp;
      } else {
	obj = (MString**)(&(*obj)->next);
      }
    }
  }

  strtab->size *= 2;
}

MString* muaS_newstr(mua_State* M, const char* str) 
{
  return muaS_newlstr(M, str, strlen(str));
}

MString* muaS_newlstr(mua_State* M, const char* str, int len)
{
  int h, idx;
  MString* s = NULL;
  struct strtable* strtab = &M->G->strtab;

  h = str_hash_value(str, len);
  idx = mod(h, strtab->size);
  for (s = strtab->table[idx]; s != NULL; s = (MString*)s->next) {
    if (len == s->len && memcmp(getstr(s), str, len) == 0) break;
  }
  
  if (s == NULL) {
    s = (MString*)muaB_newobj(M, MUA_STRING, sizeof(MString) + len + 1);
    memcpy(getstr(s), str, len);
    *(getstr(s)+len) = '\0';

    s->next = strtab->table[idx];
    s->hash = h;
    s->flag = 0;
    s->len = len;
    strtab->table[idx] = s;
    ++strtab->total;

    if (strtab->total > strtab->size) resize_strtable(strtab);
  }

  return s;
}

#ifdef TEST_MSTRING
#include <stdio.h>
#include <time.h>
#define PASS printf("pass test\n")
#define FAIL printf("fail test\n")

static void dump_all_string(mua_State* M)
{
  int i, cnt = 0;
  MString* str;
  struct strtable *strtab = &M->G->strtab;
  
  puts("dump_all_string:");
  for (i = 0; i < strtab->size; ++i) {
    for (str = strtab->table[i]; str != NULL; str = (MString*)str->next) {
      puts(getstr(str));
      ++cnt;
    }
  }

  printf("total different string %d\n", cnt);
}

int main()
{
  mua_State* M = mua_newstate();

  MString* s[4];
  s[0] = muaS_newstr(M, "abc");
  s[1] = muaS_newstr(M, "abc");
  s[2] = muaS_newstr(M, "ab");
  if (equstr(s[0], s[1])) 
    PASS;
  else
    FAIL;
  if (!equstr(s[0], s[2])) PASS; else FAIL;
  if (!equstr(s[1], s[2])) PASS; else FAIL;

  int N = 10010, L = 100;
  char str[110] = {0};
  int i, j;
  MString *p, *q;
  srand(time(NULL));
  for (i = 0; i < N; ++i) {
    for (j = 0 ; j < L; ++j) str[j] = 'a' + rand()%26;
    p = muaS_newstr(M, str);
    if (rand()&1) {
      q = muaS_newstr(M, getstr(p));
      if (equstr(p, q)) PASS; else {
	FAIL; 
	printf("%d %d\n", p->len, q->len);
	printf("%s\n%s\n", getstr(p), getstr(q));
      }
    }
  }

  dump_all_string(M);

  return 0;
}
#endif
