#include <stdlib.h>
#include <string.h>
#include "mvm.h"
#include "mopcode.h"
#include "mdo.h"
#include "mtable.h"
#include "mstate.h"
#include "mstring.h"

#define START_ENTRY goto _START_ENTRY
#define NEXT_INSTRUCTION goto _NEXT_INSTRUCTION
#define FUNCTOIN_RETURN goto _FUNCTION_RETURN

void muaV_arith(mua_State* M, int a)
{
  int i, j;
  MValue* o;
  MValue* p; 

  switch (a) {
  case  A_ARITH_OR:
  case  A_ARITH_AND:
    o = stackidxvalue(M, M->top-2);
    p = stackidxvalue(M, M->top-1);
    mua_assert(objbool(o));
    mua_assert(objbool(p));
    i = valuecov2bool(o);
    j = valuecov2bool(p);
    if (a == A_ARITH_OR) 
      setboolvalue(o, i|j)
    else 
      setboolvalue(o, i&j)
    dectop(M, 1);
    break;

  case  A_ARITH_LESS:
  case  A_ARITH_GREATER:
  case  A_ARITH_LESSEQU:
  case  A_ARITH_GREATEREQU:
  case  A_ARITH_EQU:
  case  A_ARITH_NOTEQU:
    o = stackidxvalue(M, M->top-2);
    p = stackidxvalue(M, M->top-1);
    mua_assert(objbool(o) || objnum(o));
    mua_assert(objbool(p) || objnum(p));
    muaB_bool_arith(o, p, o, a);
    dectop(M, 1);
    break;

  case A_ARITH_NOT:
  case A_ARITH_NEG:
    o = stacktopvalue(M);
    mua_assert(objbool(o) || objnum(o));
    muaB_unary_arith(o, o, a);
    break;

  case A_ARITH_POW:
  case A_ARITH_ADD:
  case A_ARITH_MINUS:
  case A_ARITH_MULT:
  case A_ARITH_DIV:
  case A_ARITH_MOD:
    o = stackidxvalue(M, M->top - 2);
    p = stackidxvalue(M, M->top - 1);
    mua_assert(objnum(o));
    mua_assert(objnum(p));
    muaB_num_arith(o, p, o, a);
    dectop(M, 1);
    break;
  }
}

void muaV_getlen(mua_State* M)
{
  MValue* o = stacktopvalue(M);
  switch (o->type) {
  case MUA_STRING:
    setnumvalue(o, value2str(o)->len);
    break;
  case MUA_TABLE:
    setnumvalue(o, muaT_length(M, value2table(o)));
    break;
  default:
    mua_assert(0);
  }
}

void muaV_concat(mua_State* M)
{
  MValue v;
  MString* ms;
  char *s1, *s2, *s;
  int len1, len2;
  
  mua_assert(objstr(stacktopvalue(M)));
  mua_assert(objstr(stackidxvalue(M, M->top-2)));
  
  s2 = getstr(value2str(stacktopvalue(M)));
  s1 = getstr(value2str(stackidxvalue(M, M->top-2)));
  len1 = strlen(s1);
  len2 = strlen(s2);

  s = (char*)malloc(sizeof(*s) * (len1+len2+1));
  strncpy(s, s1, len1);
  strncpy(s+len1, s2, len2);
  s[len1+len2] = '\0';
  
  ms = muaS_newlstr(M, s, len1+len2);
  free(s);
  setstrvalue(&v, ms);

  dectop(M, 2);
  pushvalue(M, &v);
}

void muaV_execute(mua_State* M)
{
  MValue tv;
  int a, b;
  MValue* v;
  MTable* t;
  MProto* p = getfuncproto(M, M->base);
  Instruction* pc = p->code;
  Instruction* endpc = p->code + p->code_len;
  int lv_base = M->top - M->base - 1;

  M->top += p->lv_len;

 _START_ENTRY:
  while (pc < endpc) {
    a = GET_A(*pc);
    b = GET_B(*pc);
    switch (GET_OP(*pc)) {
    case OP_POP:
      dectop(M, a);
      NEXT_INSTRUCTION;
      
    case OP_PUSHCONST:
      pushvalue(M, p->k + b);
      NEXT_INSTRUCTION;

    case OP_PUSHGLOBAL:
      v = muaT_get(M, M->G->g_env, p->k + b);
      if (v == NULL) pushnil(M); else pushvalue(M, v);
      NEXT_INSTRUCTION;

    case OP_PUSHARG:
      v = stackargvalue(M, b);
      pushvalue(M, v);
      NEXT_INSTRUCTION;

    case OP_PUSHLOCAL:
      v = stackargvalue(M, lv_base + b);
      pushvalue(M, v);
      NEXT_INSTRUCTION;

    case OP_SETLOCAL:
      *stackargvalue(M, lv_base + b) = *stacktopvalue(M);
      dectop(M, 1);
      NEXT_INSTRUCTION;

    case OP_SETGLOBAL:
      muaT_set(M, M->G->g_env, p->k + b, stacktopvalue(M));
      dectop(M, 1);
      NEXT_INSTRUCTION;

    case OP_ARITH:
      muaV_arith(M, a);
      NEXT_INSTRUCTION;

    case OP_GETLEN:
      muaV_getlen(M);
      NEXT_INSTRUCTION;

    case OP_CONCAT:
      muaV_concat(M);
      NEXT_INSTRUCTION;

    case OP_GETTABLE: //pattern:key table. For easy parse
      mua_assert(objtable(stackidxvalue(M, M->top-2)));
      v = muaT_get(M, value2table(stackidxvalue(M, M->top-2)), stacktopvalue(M));
      dectop(M, 2);
      if (v == NULL) pushnil(M); else pushvalue(M, v);
      NEXT_INSTRUCTION;

    case OP_SETTABLE: //patter:value key table. For easy parse
      mua_assert(objtable(stackidxvalue(M, M->top-3)));
      muaT_set(M, value2table(stackidxvalue(M, M->top-3)), stackidxvalue(M, M->top-2), stacktopvalue(M));
      dectop(M, 3);
      NEXT_INSTRUCTION;

    case OP_NEWTABLE:
      t = muaT_new(M);
      settablevalue(&tv, t);
      pushvalue(M, &tv);
      NEXT_INSTRUCTION;
      
    case OP_CALL:
      muaD_call(M, a);
      NEXT_INSTRUCTION;
      
    case OP_RETURN:
      muaD_return(M);
      FUNCTOIN_RETURN;

    case OP_JMP:
      v = stacktopvalue(M);
      if (a || objnil(v) || (objbool(v) && !value2bool(v))) {
	pc = p->code + b;
	dectop(M, 1);
	START_ENTRY;
      } else {
	dectop(M, 1);
	NEXT_INSTRUCTION;
      }
    }

  _NEXT_INSTRUCTION:
    ++pc;
  }

 _FUNCTION_RETURN:
  return;
}
