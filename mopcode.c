#include "mopcode.h"

static const char* opcode_str[] = {
  "OP_POP",
  "OP_PUSHLOCAL",
  "OP_PUSHCONST",
  "OP_PUSHGLOBAL",
  "OP_PUSHARG",
  "OP_SETGLOBAL",
  "OP_SETLOCAL",
  "OP_ARITH",
  "OP_GETLEN",
  "OP_CONCAT",
  "OP_GETTABLE",
  "OP_SETTABLE",
  "OP_NEWTABLE",
  "OP_CALL",
  "OP_RETURN",
  "OP_JMP",
};

const char* muaO_get_opstr(mua_State* M, Instruction c)
{
  if (c >= NUM_OPCODE) return NULL;
  return opcode_str[c];
}
