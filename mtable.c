#include <stdlib.h>
#include <string.h>
#include "mstate.h"
#include "mtable.h"

#define mod(x,y) ((x)&((y)-1))

static void resize_table(mua_State* M, MTable* tab)
{
  int i;
  struct MNode** p;
  struct MNode* tmp;
  tab->table = (struct MNode**)realloc(tab->table, sizeof(struct MNode*)* 2*tab->size);
  memset(tab->table + tab->size, 0, sizeof(struct MNode*) * tab->size);

  for (i = 0; i < tab->size; ++i) {
    p = &tab->table[i];
    while (*p != NULL) {
      if (muaB_gethash(M, &((*p)->key)) & tab->size) {
	tmp = (*p)->next;
	(*p)->next = tab->table[i+tab->size];
	tab->table[i+tab->size] = (*p);
	(*p) = tmp;
      } else {
	p = (struct MNode**)(&(*p)->next);
      }
    }
  }

  tab->size *= 2;
}

MTable* muaT_new(mua_State* M)
{
  MTable* tab = (MTable*)muaB_newobj(M, MUA_TABLE, sizeof(*tab));
  tab->size = MIN_TABLE_SIZE;
  tab->total = 0;
  tab->table = (struct MNode**)calloc(sizeof(struct MNode*), MIN_TABLE_SIZE);
  return tab;
}

void muaT_next(mua_State* M, MTable* tab, MValue* key, MValue** k, MValue** v)
{
  int idx = 0;
  struct MNode* p;
  if (!objnil(key)) idx = mod(muaB_gethash(M, key), tab->size);
  p = tab->table[idx];
  for (; p != NULL; p = p->next) {
    if (muaB_checkequ(M, &(p->key), key)) break;
  }
  if (p != NULL) p = p->next; else p = tab->table[idx];

  *k = *v = NULL;
  if (p != NULL) {
    *k = &p->key;
    *v = &p->value;
  } else {
    for (++idx; idx < tab->size; ++idx) {
      if (tab->table[idx] != NULL) {
	*k = &tab->table[idx]->key;
	*v = &tab->table[idx]->value;
	break;
      }
    }
  }
  
}

void muaT_set(mua_State* M, MTable* tab, MValue* key, MValue* value)
{
  if (objnil(key)) return;

  int h = muaB_gethash(M, key);
  int idx = mod(h, tab->size);
  struct MNode* p = tab->table[idx];
  for (; p != NULL; p = p->next) {
    if (muaB_checkequ(M, &(p->key), key)) break;
  }

  if (p == NULL) {
    p = (struct MNode*)malloc(sizeof(*p));
    p->key = *key;
    p->next = tab->table[idx];
    tab->table[idx] = p;
    ++tab->total;
    
    if (tab->total > tab->size) resize_table(M, tab);
  }
  
  p->value = *value; //may lead to memory leak, solve is later
}

MValue* muaT_get(mua_State* M, MTable* tab, MValue* key)
{
  if (objnil(key)) return NULL;

  int h = muaB_gethash(M, key);
  int idx = mod(h, tab->size);
  struct MNode* p = tab->table[idx];
  for (; p != NULL; p = p->next) {
    if (muaB_checkequ(M, &p->key, key)) break;
  }

  if (p == NULL) return NULL;
  return &p->value;
}

int muaT_length(mua_State* M, MTable* tab)
{
  int i;
  MValue o;
  for (i = 1; ; ++i) {
    setnumvalue(&o, i);
    if (muaT_get(M, tab, &o) == NULL) break;
  }

  return i-1;
}

#ifdef TEST_MTABLE
int main()
{
}
#endif
