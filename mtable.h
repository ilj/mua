#ifndef MTABLE_
#define MTABLE_

#include "mobject.h"

MTable* muaT_new(mua_State* M);
void muaT_next(mua_State* M, MTable* tab, MValue* key, MValue** k, MValue** v);
void muaT_set(mua_State* M, MTable* tab, MValue* key, MValue* value);
MValue* muaT_get(mua_State* M, MTable* tab, MValue* key);
int muaT_length(mua_State* M, MTable* tab);


#endif
