#include <stdlib.h>
#include "mcode.h"
#include "mfunction.h"
#include "mopcode.h"

static void check_code_array(MProto* p)
{
  if (p->code_len >= p->code_size) {
    p->code_size *= 2;
    p->code = (Instruction*)realloc(p->code, sizeof(Instruction)*p->code_size);
  }
}

static void check_k_array(MProto* p) {
  if (p->k_len >= p->k_size) {
    p->k_size *= 2;
    p->k = (MValue*)realloc(p->k, sizeof(MValue)*p->k_size);
  }
}

int muaC_addcode(MProto* p, int op, int a, int b)
{
  check_code_array(p);
  p->code[p->code_len] = MAKE_OPCODE(op,a,b);
  return p->code_len++;
}

int muaC_addk(MProto* p, MValue* v)
{
  check_k_array(p);
  p->k[p->k_len] = *v;
  return p->k_len++;
}

void muaC_addunop(MProto* p, int unop)
{
  switch (unop) {
  case OPR_MINUS: muaC_addcode(p, OP_ARITH, A_ARITH_NEG, 0); break;
  case OPR_NOT: muaC_addcode(p, OP_ARITH, A_ARITH_NOT, 0); break;
  case OPR_LEN: muaC_addcode(p, OP_GETLEN, 0, 0); break;
  default: mua_assert(0);
  }
}

void muaC_addbinop(MProto* p, int binop)
{
  switch (binop) {
  case OPR_ADD: muaC_addcode(p, OP_ARITH, A_ARITH_ADD, 0); break;
  case OPR_SUB: muaC_addcode(p, OP_ARITH, A_ARITH_MINUS, 0); break;
  case OPR_MUL: muaC_addcode(p, OP_ARITH, A_ARITH_MULT, 0); break;
  case OPR_DIV: muaC_addcode(p, OP_ARITH, A_ARITH_DIV, 0); break;
  case OPR_MOD: muaC_addcode(p, OP_ARITH, A_ARITH_MOD, 0); break;
  case OPR_POW: muaC_addcode(p, OP_ARITH, A_ARITH_POW, 0); break;
  case OPR_CONCAT: muaC_addcode(p, OP_CONCAT, 0, 0); break;
  case OPR_EQ: muaC_addcode(p, OP_ARITH, A_ARITH_EQU, 0); break;
  case OPR_LT: muaC_addcode(p, OP_ARITH, A_ARITH_LESS, 0); break;
  case OPR_LE: muaC_addcode(p, OP_ARITH, A_ARITH_LESSEQU, 0); break;
  case OPR_NE: muaC_addcode(p, OP_ARITH, A_ARITH_NOTEQU, 0); break;
  case OPR_GT: muaC_addcode(p, OP_ARITH, A_ARITH_GREATER, 0); break;
  case OPR_GE: muaC_addcode(p, OP_ARITH, A_ARITH_GREATEREQU, 0); break;
  case OPR_AND: muaC_addcode(p, OP_ARITH, A_ARITH_AND, 0); break;
  case OPR_OR: muaC_addcode(p, OP_ARITH, A_ARITH_OR, 0); break;
  default:
    mua_assert(0);
  }
}

void muaC_setA(MProto* p, int cidx, int a)
{
  mua_assert(cidx < p->code_len);
  Instruction *pc = p->code + cidx;
  *pc = SET_A(*pc, a);
}

void muaC_setB(MProto* p, int cidx, int b)
{
  mua_assert(cidx < p->code_len);
  Instruction *pc = p->code + cidx;
  *pc = SET_B(*pc, b);
}
