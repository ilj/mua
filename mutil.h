#ifndef MUTIL_
#define MUTIL_

#include <string.h>

#define MAX_BUFFERSIZE 64

#define lislalpha(c)	(isalpha(c) || (c) == '_')
#define lislalnum(c)	(isalnum(c) || (c) == '_')
#define lisdigit(c)	(isdigit(c))
#define lisspace(c)	(isspace(c))
#define lisprint(c)	(isprint(c))
#define lisxdigit(c)	(isxdigit(c))

typedef struct Buffer {
  char *p;
  int size, len;
} Buffer;

#define mua_init_buffer(buff) ((buff)->p = (char*)calloc(MAX_BUFFERSIZE, 1), (buff)->size = MAX_BUFFERSIZE, (buff)->len = 0)
#define mua_clear_buffer(buff) ((buff)->len = (buff)->size)
#define mua_close_buffer(buff) (free((buff)->p), (buff)->p = NULL)
#define mua_extend_buffer(buff) ((buff)->size *= 2, (buff)->p = (char*)realloc((buff)->p, (buff)->size))

typedef int (*Mua_Reader)(Buffer* buff, void** data);

typedef struct MIO {
  Mua_Reader reader;
  Buffer* buff;
  void* data;
} MIO;

int MIO_stdin_reader(Buffer* buff, void** data);

#endif
