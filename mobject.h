#ifndef MOBJECT_
#define MOBJECT_

#include "mua.h"

#define CommonHeader int type; void* next

union Mua_Values
{
  void* obj;
  double n;
  int b;
  Mua_CFunction cfunc;
};

typedef struct MValue
{
  union Mua_Values v;
  int type;
} MValue;

typedef struct MObject {
  CommonHeader;
} MObject;

typedef struct MString {
  CommonHeader;
  int len;
  int flag;
  int hash;
} MString;

struct MNode {
  MValue key;
  MValue value;
  struct MNode* next;
};

typedef struct MTable {
  CommonHeader;
  struct MNode** table;
  int size;
  int total;
} MTable;

typedef struct MProto {
  Instruction* code;
  MValue* k;
  int k_size, k_len;
  int lv_len;
  int code_size, code_len;
} MProto;

typedef struct MFunction {
  CommonHeader;
  union {
    Mua_CFunction c_func;
    MProto* m_func;
  } func;
} MFunction;


MObject* muaB_newobj(mua_State* M, int type, int size);
int muaB_checkequ(mua_State* M, MValue* a, MValue* b);
int muaB_gethash(mua_State* M, MValue* o);

void muaB_unary_arith(MValue* o, MValue* result, int op);
void muaB_bool_arith(MValue* o, MValue* p, MValue* result, int op);
void muaB_num_arith(MValue* o, MValue* p, MValue* result, int op);

void muaB_str2d(const char* s, int len, double *n);

#define objnil(x) ((x)->type == MUA_NIL)
#define objnum(x) ((x)->type == MUA_NUMBER)
#define objbool(x) ((x)->type == MUA_BOOL)
#define objstr(x) ((x)->type == MUA_STRING)
#define objtable(x) ((x)->type == MUA_TABLE)
#define objfunc(x) ((x)->type & MUA_FUNCTION)
#define objmfunc(x) ((x)->type & MUA_MFUNCTION)
#define objcfunc(x) ((x)->type & MUA_CFUNCTION)

#define setnil(x) ((x)->type = MUA_NIL)

#define valuecov2bool(x) ((x)->type != MUA_NIL && ((x)->type != MUA_BOOL || (x)->v.b))

#define value2num(x) ((x)->v.n)
#define value2integer(x) ((int)((x)->v.n))
#define value2bool(x) ((x)->v.b)
#define value2str(x) ((MString*)((x)->v.obj))
#define value2table(x) ((MTable*)((x)->v.obj))
#define value2func(x) ((MFunction*)((x)->v.obj))
#define value2obj(x) ((x)->v.obj)

#define setnumvalue(vv, nn) { (vv)->type = MUA_NUMBER; (vv)->v.n = (nn); }
#define setboolvalue(vv, bb) { (vv)->type = MUA_BOOL; (vv)->v.b = (bb); }
#define setstrvalue(vv, ss) { (vv)->type = MUA_STRING; (vv)->v.obj = (ss); }
#define settablevalue(vv, tt) { (vv)->type = MUA_TABLE; (vv)->v.obj = (tt); }
#define setfuncvalue(vv, ff) { (vv)->type = MUA_FUNCTION; (vv)->v.obj = (ff); }
#define setobjvalue(vv, oo) { (vv)->type = (oo)->type; (vv)->v.obj = (oo); }

#endif
