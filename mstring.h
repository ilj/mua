#ifndef MSTRING_
#define MSTRING_

#include "mobject.h"

#define getstr(s) ((char*)(s) + sizeof(MString))

#define equstr(a,b) ((a) == (b))

MString* muaS_newlstr(mua_State* M, const char* str, int len);
MString* muaS_newstr(mua_State* M, const char* str);

#endif
