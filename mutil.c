#include <stdio.h>
#include <string.h>
#include "mutil.h"

int MIO_stdin_reader(Buffer* buff, void** data)
{
  char* p = *(char**)data;
  if (p == NULL || p >= buff->p + buff->len) {
    if (NULL == fgets(buff->p, buff->size, stdin)) return EOF;
    buff->len = strlen(buff->p);
    *data = buff->p;
  }
  
  return buff->len;
}
