#ifndef MOPCODE_
#define MOPCODE_

#include "mua.h"

typedef enum {
  OP_POP = 0,
  OP_PUSHLOCAL,
  OP_PUSHCONST,
  OP_PUSHGLOBAL,
  OP_PUSHARG,
  OP_SETGLOBAL,
  OP_SETLOCAL,
  OP_ARITH,
  OP_GETLEN,
  OP_CONCAT,
  OP_GETTABLE,
  OP_SETTABLE,
  OP_NEWTABLE,
  OP_CALL,
  OP_RETURN,
  OP_JMP,
} OpCode;

enum OPCODE_A {
  A_ARITH_OR = 0,
  A_ARITH_AND,
  A_ARITH_LESS,
  A_ARITH_GREATER,
  A_ARITH_LESSEQU,
  A_ARITH_GREATEREQU,
  A_ARITH_NOT,
  A_ARITH_NEG,
  A_ARITH_ADD,
  A_ARITH_MINUS,
  A_ARITH_MULT,
  A_ARITH_DIV,
  A_ARITH_MOD,
  A_ARITH_POW,
  A_ARITH_NOTEQU,
  A_ARITH_EQU,
};


#define NUM_OPCODE ((int)OP_JMP+1)
#define OPCODE_SIZE 32
#define OP_SIZE 8
#define A_SIZE 12
#define B_SIZE 12
#define AB_SIZE (A_SIZE + B_SIZE)
#define OP_MASK ((1 << OP_SIZE)-1)
#define A_MASK ((1 << A_SIZE)-1)
#define B_MASK ((1 << B_SIZE)-1)

#define GET_OP(c) (((c) >> AB_SIZE) & OP_MASK)
#define GET_A(c) (((c) >> B_SIZE) & A_MASK)
#define GET_B(c) ((c) & B_MASK)

#define SET_OP(c,op) (((c) & (~(OP_MASK << AB_SIZE))) | (op << AB_SIZE))
#define SET_A(c,a) (((c) & (~(A_MASK << B_SIZE))) | (a << B_SIZE))
#define SET_B(c,b) (((c) & (~B_MASK)) | b)

#define MAKE_OPCODE(op,a,b) \
  ((((op)&OP_MASK) << AB_SIZE) | (((a)&A_MASK) << B_SIZE) | ((b)&B_MASK))

const char* muaO_get_opstr(mua_State* M, Instruction c);

#endif
