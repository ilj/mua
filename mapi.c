#include <string.h>
#include <stdio.h>
#include "mapi.h"
#include "mparser.h"
#include "mfunction.h"
#include "mstring.h"
#include "mtable.h"
#include "mstate.h"
#include "mdo.h"
#include "mopcode.h"
#include "mutil.h"

static void dump_value(mua_State* M, MValue* v)
{
  printf("  ");
  if (objnil(v)) {
    printf("NIL\n");
  } else if (objbool(v)) {
    printf("%s\n", value2bool(v) ? "True" : "False");
  } else if (objnum(v)) {
    printf("%.0lf\n", value2num(v));
  } else if (objstr(v)) {
    printf("\"%s\"\n", getstr(value2str(v)));
  } else if (objtable(v)) {
    printf("table\n");
  } else if (objfunc(v)) {
    printf("function\n");
  } else {
    printf("unknown\n");
  }
}

static void dump_code(mua_State* M, Instruction c)
{
  int op = GET_OP(c);
  int a = GET_A(c);
  int b = GET_B(c);
  const char* ops = muaO_get_opstr(M, op);
  if (ops == NULL) { printf("  opcode invalid"); return; }
  printf("  %s %d %d\n", muaO_get_opstr(M, op), a, b);
}


int mua_next(mua_State* M, int idx)
{
  MValue *k, *v;
  muaT_next(M, value2table(stackargvalue(M, idx)), stacktopvalue(M), &k, &v);
  if (k == NULL) {
    setnil(stacktopvalue(M));
    return 0;
  } else {
    *stacktopvalue(M) = *k;
    pushvalue(M, v);
    return 1;
  }
}

int mua_isnil(mua_State* M, int idx) 
{
  return objnil(stackargvalue(M, idx));
}

int mua_isbool(mua_State* M, int idx)
{
  return objbool(stackargvalue(M, idx));
}

int mua_isnumber(mua_State* M, int idx) 
{
  return objnum(stackargvalue(M, idx));
}

int mua_isstring(mua_State* M, int idx)
{
  return objstr(stackargvalue(M, idx));
}
int mua_istable(mua_State* M, int idx)
{
  return objtable(stackargvalue(M, idx));
}

int mua_isfunction(mua_State* M, int idx)
{
  return objfunc(stackargvalue(M, idx));
}

int mua_gettop(mua_State* M)
{
  return M->top - M->base - 1;
}

void mua_pop(mua_State* M, int size)
{
  dectop(M, size);
}

void mua_pushvalue(mua_State* M, int idx)
{
  MValue* v = stackargvalue(M, idx);
  pushvalue(M, v);
}

void mua_pushnil(mua_State* M)
{
  inctop(M);
  setnil(stacktopvalue(M));
}

void mua_pushnumber(mua_State* M, double num)
{
  inctop(M);
  setnumvalue(stacktopvalue(M), num);
}

void mua_pushlstring(mua_State* M, const char* str, int len)
{
  MString* s = muaS_newlstr(M, str, len);
  inctop(M);
  setstrvalue(stacktopvalue(M), s);
}

void mua_pushstring(mua_State* M, const char* str)
{
  mua_pushlstring(M, str, strlen(str));
}


void mua_pushcfunction(mua_State* M, Mua_CFunction func)
{
  MFunction* f = muaF_newC(M, func);
  inctop(M);
  setfuncvalue(stacktopvalue(M), f);
}

const char* mua_tolstring(mua_State* M, int idx, int* len)
{
  MString* s = value2str(stackargvalue(M, idx));
  if (len != NULL) *len = s->len;
  return getstr(s);
}

const char* mua_tostring(mua_State* M, int idx)
{
  MString* s = value2str(stackargvalue(M, idx));
  return getstr(s);
}

double mua_tonumber(mua_State* M, int idx)
{
  return value2num(stackargvalue(M, idx));
}

void mua_gettablen(mua_State* M, int idx, int key)
{
  MTable* t = value2table(stackargvalue(M, idx));
  MValue k, *pv;
  setnumvalue(&k, key);
  pv = muaT_get(M, t, &k);
  if (pv == NULL) pushnil(M); else pushvalue(M, pv);
}

void mua_settablen(mua_State* M, int idx, int tidx, int key)
{
  MTable* t = value2table(stackargvalue(M, idx));
  MValue k, *pv;
  setnumvalue(&k, key);
  pv = stackargvalue(M, tidx);
  muaT_set(M, t, &k, pv);
}

int mua_getlen(mua_State* M, int idx)
{
  MValue* o = stackargvalue(M, idx);
  switch (o->type) {
  case MUA_STRING:
    return value2str(o)->len;
  case MUA_TABLE:
    return muaT_length(M, value2table(o));
  default:
    mua_assert(0);
  }
}

int mua_getvaluetype(mua_State* M, int idx)
{
  return stackargvalue(M, idx)->type;
}

int mua_tobool(mua_State* M, int idx)
{
  return value2bool(stackargvalue(M, idx));
}

void mua_call(mua_State* M, int total)
{
  muaD_call(M, total);
}

void mua_load(mua_State* M)
{
  ParseState ps;
  MFunction* f = muaF_newM(M);
  MValue v;
  LexState ls;
  Buffer buff[2];
  MIO io;
  
  mua_init_buffer(&buff[0]);
  mua_clear_buffer(&buff[0]);
  mua_init_buffer(&buff[1]);
  io.data = NULL;
  io.buff = &buff[0];
  io.reader = MIO_stdin_reader;
  muaX_init(&ls, &io, &buff[1]);

  ps.M = M;
  ps.k_env = muaT_new(M);
  ps.p = f->func.m_func;
  ps.ls = &ls;
  ps.lv = (LocalVarInfo*)malloc(sizeof(*ps.lv) * MIN_TABLE_SIZE);
  ps.lv_size = MIN_TABLE_SIZE;
  ps.lv_len = 0;
  ps.blk = NULL;

  muaP_parse_simple_code(&ps);

  setfuncvalue(&v, f);
  pushvalue(M, &v);
}

void mua_dump_mfunction(mua_State* M)
{
  int i;
  MProto* p;
  MValue* v = stacktopvalue(M);
  if (!objmfunc(v)) {
    printf("value is not a mua function\n");
    return;
  }

  p = value2func(v)->func.m_func;
  
  printf("dump k:\n");
  for (i = 0; i < p->k_len; ++i) dump_value(M, p->k+i);
  puts("");

  printf("dump code:\n");
  for (i = 0; i < p->code_len; ++i) {
    printf("%d: ", i);
    dump_code(M, p->code[i]);
  }
  puts("");
}

void mua_openlib(mua_State* M, const char* libname, MuaLib* lib)
{
  int i;
  MTable* t = M->G->g_env;
  MValue v, k;
  if (libname != NULL) {
    setstrvalue(&k, muaS_newstr(M, libname));
    settablevalue(&v, muaT_new(M));
    muaT_set(M, t, &k, &v);
    t = value2table(&v);
  }

  for (i = 0; lib[i].func_name != NULL; ++i) {
    setstrvalue(&k, muaS_newstr(M, lib[i].func_name));
    setfuncvalue(&v, muaF_newC(M, lib[i].func));
    muaT_set(M, t, &k, &v);
  }
}
