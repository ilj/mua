#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mbaselib.h"

#define PI 3.1415926535897932384626433832795
#define num_equ(a, b) (fabs((a)-(b)) < 1e-9)
#define max(a,b) ((a) < (b) ? (b) : (a))
#define min(a,b) ((a) > (b) ? (b) : (a))

static int print(mua_State* M)
{
  mua_assert(mua_gettop(M) == 1);

  if (mua_isnil(M, 1)) {
    printf("nil\n");
  } else if (mua_isbool(M, 1)) {
    printf("%s\n", mua_tobool(M, 1) ? "true" : "false");
  } else if (mua_isnumber(M, 1)) {
    printf("%.9lf\n", mua_tonumber(M, 1));
  } else if (mua_isstring(M, 1)) {
    printf("%s\n", mua_tostring(M, 1));
  } else if (mua_istable(M, 1)) {
    printf("table\n");
  } else if (mua_isfunction(M, 1)) {
    printf("function\n");
  } else {
    puts("unknown");
  }

  return 0;
}

static int tonumber(mua_State* M)
{
  const char* s;
  char *endptr;
  double n;
  int len;
  mua_assert(mua_gettop(M) == 1);

  if (mua_isstring(M, 1)) {
    s = mua_tolstring(M, 1, &len);
    n = strtod(s, &endptr);
    if ((int)(endptr - s) != len) {
      mua_pushnil(M);
    } else {
      mua_pushnumber(M, n);
    }
  } else if (!mua_isnumber(M, 1)) {
    mua_pushnil(M);
  }

  return 1;
}

static int tostring(mua_State* M)
{
  mua_assert(mua_gettop(M) == 1);
  
  if (mua_isfunction(M, 1)) {
    mua_pushstring(M, "function");
  } else if (mua_istable(M, 1)) {
    mua_pushstring(M, "table");
  } else if (mua_isnumber(M, 1)) {
    char s[30];
    sprintf(s, "%.14g", mua_tonumber(M, 1));
    mua_pushstring(M, s);
  } else if (mua_isbool(M, 1)) {
    mua_pushstring(M, mua_tobool(M, 1) ? "true" : "false");
  }

  return 1;
}

static MuaLib baselib[] = {
  { "print", print },
  { "tonumber", tonumber },
  { "tostring", tostring },
  { NULL, NULL },
};

static int rep(mua_State* M)
{
  int i, n, len;
  const char* src;
  char *dst;

  mua_assert(mua_gettop(M) == 2);
  
  src = mua_tolstring(M, 1, &len);
  n = (int)mua_tonumber(M, 2);
  
  dst = (char*)malloc(sizeof(*dst) * n * len + 1);
  for (i = 0; i < n; ++i) 
    memcpy(dst+len*i, src, sizeof(*dst)*len);
  dst[len*n] = '\0';

  mua_pushstring(M, dst);

  free(dst);

  return 1;
}

static int sub(mua_State* M)
{
  const char* s;
  int i, j, len;
  int top = mua_gettop(M);
  
  mua_assert(2 <= top && top <= 3);
  s = mua_tolstring(M, 1, &len);
  i = (int)mua_tonumber(M, 2);
  if (i < 0) i += len; else i--;
  if (top == 3) j = (int)mua_tonumber(M, 3); else j = len;
  if (j < 0) j += len; else j--;

  if (i < j) {
    i = max(0, i); j = min(len-1, j);
  }
  if (i > j) {
    mua_pushstring(M, "\0");
  } else {
    char *dst = (char*)malloc(sizeof(*dst) * (j-i+2));
    dst[j-i+1] = '\0';
    for (; i <= j; --j) dst[j-i] = s[j];
    mua_pushstring(M, dst);
    free(dst);
  }

  return 1;
}

static MuaLib stringlib[] = {
  { "rep", rep },
  { "sub", sub },
  { NULL, NULL },
};

static int concat(mua_State* M)
{
  const char* sep = "";
  const char* s;
  char* dst;
  int top = mua_gettop(M);
  int len, tlen, slen = 0;
  int i, l, size;
  
  mua_assert(1 <= top && top <= 2);
  len = mua_getlen(M, 1);
  if (top == 2) sep = mua_tolstring(M, 2, &slen);

  size = slen*(len-1);
  for (i = 1; i <= len; ++i) {
    mua_gettablen(M, 1, i);
    size += mua_getlen(M, top+1);
    mua_pop(M, 1);
  }

  dst = (char*)malloc(sizeof(*dst) * (size+1));
  l = 0;
  for (i = 1; i <= len; ++i) {
    mua_gettablen(M, 1, i);
    s = mua_tolstring(M, top+1, &tlen);
    memcpy(dst+l, s, sizeof(*dst) * tlen);
    mua_pop(M, 1);
    l += tlen;
    if (i != len) {
      memcpy(dst+l, sep, sizeof(*dst) * slen);
      l += slen;
    }
  }
  dst[l] = '\0';
  
  mua_pushstring(M, dst);
  free(dst);

  return 1;
}

static int sort_cmp(mua_State* M, int idx, int mid_idx, int cmp_idx) // -1 0 1
{
  int res = 0;
  int lv_idx;
  mua_gettablen(M, 1, idx);
  lv_idx = mua_gettop(M);
  
  if (cmp_idx != -1) {
    mua_pushvalue(M, cmp_idx);
    mua_pushvalue(M, lv_idx);
    mua_pushvalue(M, mid_idx);
    mua_call(M, 2);
    res -= mua_tobool(M, mua_gettop(M));
    mua_pop(M, 1);

    mua_pushvalue(M, cmp_idx);
    mua_pushvalue(M, mid_idx);
    mua_pushvalue(M, lv_idx);
    mua_call(M, 2);
    res += mua_tobool(M, mua_gettop(M));
    mua_pop(M, 1);
    
  } else {
    mua_assert(mua_getvaluetype(M, lv_idx) == mua_getvaluetype(M, mid_idx));
    switch (mua_getvaluetype(M, lv_idx)) {
    case MUA_NUMBER:
      if (num_equ(mua_tonumber(M, lv_idx), mua_tonumber(M, mid_idx))) 
	res = 0;
      else
	res = mua_tonumber(M, lv_idx) < mua_tonumber(M, mid_idx) ? -1 : 1;
      break;
    case MUA_STRING:
      res = strcmp(mua_tostring(M, lv_idx), mua_tostring(M, mid_idx));
      break;
    default:
      mua_assert(0);
    }
  }

  mua_pop(M, 1);

  return res;
}

static void sort_swap(mua_State* M, int i, int j)
{
  int arg_idx;
  mua_gettablen(M, 1, i);
  mua_gettablen(M, 1, j);
  arg_idx = mua_gettop(M);
  
  mua_settablen(M, 1, arg_idx, i);
  mua_settablen(M, 1, arg_idx-1, j);

  mua_pop(M, 2);
}

static void sort_divide(mua_State* M, int i, int j, int* ll, int *rr, int cmp_idx)
{
  int mid_idx;
  mua_gettablen(M, 1, (i+j)/2);
  mid_idx = mua_gettop(M);

  while (i < j) {
    while (i < j && sort_cmp(M, i, mid_idx, cmp_idx) < 0) ++i;
    while (i < j && sort_cmp(M, j, mid_idx, cmp_idx) > 0) --j;
    if (i < j) {
      sort_swap(M, i++, j--);
    }
  }

  if (i == j) {
    if (sort_cmp(M, i, mid_idx, cmp_idx) < 0)
      *ll = j, *rr = i+1;
    else if (sort_cmp(M, i, mid_idx, cmp_idx) > 0)
      *ll = j-1, *rr = i;
    else
      *ll = j-1, *rr = i+1;
  } else {
    *ll = j; *rr = i;
  }

  mua_pop(M, 1);
}

static void sort_quick(mua_State*M, int i, int j, int cmp_idx)
{
  int ll, rr;
  while (i < j) {
    sort_divide(M, i, j, &ll, &rr, cmp_idx);
    mua_assert(mua_gettop(M) <= 2);
    if (i < ll) 
      sort_quick(M, i, ll, cmp_idx);
    i = rr;
  }
}

static int sort(mua_State* M)
{
  int top = mua_gettop(M);
  int n;

  mua_assert(1 <= top && top <= 2);
  n = mua_getlen(M, 1);

  sort_quick(M, 1, n, top == 2 ? 2 : -1);

  return 1;
}

static MuaLib tablelib[] = {
  { "concat", concat },
  { "sort", sort },
  { NULL, NULL },
};

static int m_abs(mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, fabs(n));
  return 1;
}

static int m_floor (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, floor(n));
  return 1;
}

static int m_ceil (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, ceil(n));
  return 1;
}

static int m_sqrt (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, sqrt(n));
  return 1;
}

static int m_exp (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, exp(n));
  return 1;
}

static int m_log(mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, log(n));
  return 1;
}

static int m_log10(mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, log10(n));
  return 1;
}

static int m_rad (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, n/180.0*PI);
  return 1;
}

static int m_deg (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, n/PI*180.0);
  return 1;
}

static int m_acos (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, acos(n));
  return 1;
}

static int m_asin (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, asin(n));
  return 1;
}

static int m_atan (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, atan(n));
  return 1;
}

static int m_atan2 (mua_State* M)
{
  double x = mua_tonumber(M, 1);
  double y = mua_tonumber(M, 2);
  mua_pushnumber(M, atan2(x, y));
  return 1;
}

static int m_cos (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, cos(n));
  return 1;
}

static int m_sin (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, sin(n));
  return 1;
}

static int m_tan (mua_State* M)
{
  double n = mua_tonumber(M, 1);
  mua_pushnumber(M, tan(n));
  return 1;
}

static int m_min (mua_State* M)
{
  double x = mua_tonumber(M, 1);
  double y = mua_tonumber(M, 2);
  mua_pushnumber(M, min(x,y));
  return 1;

}

static int m_max (mua_State* M)
{
  double x = mua_tonumber(M, 1);
  double y = mua_tonumber(M, 2);
  mua_pushnumber(M, max(x, y));
  return 1;
}


static MuaLib mathlib[] = {
  { "abs", m_abs },
  { "floor", m_floor },
  { "ceil", m_ceil },
  { "sqrt", m_sqrt },
  { "exp", m_exp },
  { "log", m_log },
  { "log10", m_log10 },
  { "rad", m_rad },
  { "deg", m_deg },
  { "acos", m_acos },
  { "asin", m_asin },
  { "atan", m_atan },
  { "atan2", m_atan2 },
  { "cos", m_cos },
  { "sin", m_sin },
  { "tan", m_tan },
  { "min", m_min },
  { "max", m_max },
};

void mua_openbase(mua_State* M)
{
  mua_openlib(M, NULL, baselib);
}

void mua_openstring(mua_State* M)
{
  mua_openlib(M, "string", stringlib);
}

void mua_opentable(mua_State* M)
{
  mua_openlib(M, "table", tablelib);
}

void mua_openmath(mua_State* M)
{
  mua_openlib(M, "math", mathlib);
}

void mua_openall(mua_State* M)
{
  mua_openbase(M);
  mua_openstring(M);
  mua_opentable(M);
  mua_openmath(M);
}
