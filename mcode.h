#ifndef MCODE_
#define MCODE_

#include "mobject.h"

typedef enum BinOpr {
  OPR_ADD, OPR_SUB, OPR_MUL, OPR_DIV, OPR_MOD, OPR_POW,
  OPR_CONCAT,
  OPR_EQ, OPR_LT, OPR_LE,
  OPR_NE, OPR_GT, OPR_GE,
  OPR_AND, OPR_OR,
  OPR_NOBINOPR,
} BinOpr;

typedef enum UnOpr { OPR_MINUS, OPR_NOT, OPR_LEN, OPR_NOUNOPR } UnOpr;

int muaC_addcode(MProto* p, int op, int a, int b);
int muaC_addk(MProto* p, MValue* v);

void muaC_addunop(MProto* p, int unop);
void muaC_addbinop(MProto* p, int binop);

void muaC_setA(MProto* p, int cidx, int a);
void muaC_setB(MProto* p, int cidx, int b);

#endif
