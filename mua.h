#ifndef MUA_
#define MUA_

#include <stdint.h>
#include <stddef.h>
#include <assert.h>

#define  MUA_NIL 1
#define  MUA_BOOL 2
#define  MUA_NUMBER 4
#define  MUA_STRING 8
#define  MUA_CFUNCTION 16
#define  MUA_MFUNCTION 32
#define  MUA_TABLE 64
#define  MUA_FUNCTION (MUA_CFUNCTION | MUA_MFUNCTION)


typedef struct mua_State mua_State;

typedef int (*Mua_CFunction)(mua_State* M);

typedef int32_t Instruction;

typedef int64_t MuaInteger;

#define mua_assert assert

/* These value must be the same as mopcode.h OPCODE_A enum */

#define MUA_OR             0
#define MUA_AND            1
#define MUA_LESS           2
#define MUA_GREATER        3
#define MUA_LESSEQU        4
#define MUA_GREATEREQU     5
#define MUA_NOT            6
#define MUA_NEG            7
#define MUA_ADD            8
#define MUA_MINUS          9
#define MUA_MULT           10
#define MUA_DIV            11
#define MUA_MOD            12
#define MUA_POW            13
#define MUA_NOTEQU         14
#define MUA_EQU            15

/*--------------------------------*/

/* api function */

mua_State* mua_newstate(void);

int mua_next(mua_State* M, int idx);
int mua_isnil(mua_State* M, int idx);
int mua_isbool(mua_State* M, int idx);
int mua_isnumber(mua_State* M, int idx);
int mua_isstring(mua_State* M, int idx);
int mua_istable(mua_State* M, int idx);
int mua_isfunction(mua_State* M, int idx);
int mua_gettop(mua_State* M);
void mua_pop(mua_State* M, int size);
void mua_pushvalue(mua_State* M, int idx);
void mua_pushnil(mua_State* M);
void mua_pushnumber(mua_State* M, double num);
void mua_pushlstring(mua_State* M, const char* str, int len);
void mua_pushstring(mua_State* M, const char* str);
void mua_pushcfunction(mua_State* M, Mua_CFunction func);
const char* mua_tolstring(mua_State* M, int idx, int* len);
const char* mua_tostring(mua_State* M, int idx);
double mua_tonumber(mua_State* M, int idx);
int mua_tobool(mua_State* M, int idx);
void mua_gettablen(mua_State* M, int idx, int key);
void mua_settablen(mua_State* M, int idx, int tidx, int key);
int mua_getlen(mua_State* M, int idx);
int mua_getvaluetype(mua_State* M, int idx);
void mua_call(mua_State* M, int total);
void mua_load(mua_State* M);
void mua_dump_mfunction(mua_State* M);

typedef struct MuaLib {
  const char* func_name;
  Mua_CFunction func;
} MuaLib;

void mua_openlib(mua_State* M, const char* libname, MuaLib* lib);
void mua_openall(mua_State* M);

/*----------------*/

#endif
