#ifndef MFUNCTION_
#define MFUNCTION_

#include "mobject.h"

MFunction* muaF_newC(mua_State* M, Mua_CFunction func);
MFunction* muaF_newM(mua_State* M);

#endif
