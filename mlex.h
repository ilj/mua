#ifndef MLEX_
#define MLEX_
#include <stdlib.h>
#include <ctype.h>
#include "mutil.h"

#define EOZ EOF

#define FIRST_RESERVED	257

enum RESERVED {
  TK_AND = FIRST_RESERVED, TK_BREAK,
  TK_DO, TK_ELSE, TK_ELSEIF, TK_END, TK_FALSE, TK_FOR, TK_FUNCTION,
  TK_GOTO, TK_IF, TK_IN, TK_LOCAL, TK_NIL, TK_NOT, TK_OR, TK_REPEAT,
  TK_RETURN, TK_THEN, TK_TRUE, TK_UNTIL, TK_WHILE,

  TK_CONCAT, TK_DOTS, TK_EQ, TK_GE, TK_LE, TK_NE, TK_DBCOLON, TK_EOS,
  TK_NUMBER, TK_NAME, TK_STRING, TK_EOL
};

#define NUM_RESERVED	(int)(TK_WHILE-FIRST_RESERVED+1)

#define save_and_next(ls)  (save(ls, ls->current), next(ls))

#define token_str(ls) ((ls)->buff->p)

typedef struct LexState {
  Buffer* buff;
  MIO* source;
  int current;
  int token;

  int line_cnt;
} LexState;

void muaX_init(LexState* ls, MIO* io, Buffer* buff);
int muaX_next(LexState* ls);
void muaX_trip_str(LexState* ls);

#endif
