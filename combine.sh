H=( mua.h mobject.h mstate.h mstring.h mtable.h mfunction.h mutil.h mlex.h mvm.h mdo.h mapi.h mcode.h mparser.h mopcode.h mbaselib.h )
C=( mstate.c mobject.c mstring.c mtable.c mfunction.c mlex.c mutil.c mvm.c mdo.c mapi.c mcode.c mparser.c mopcode.c mbaselib.c )

echo > combine.c

for i in ${H[@]}; do
    echo "$i"
    sed '/\#ifndef/'d $i | sed '/\#endif/'d | sed '/\#include \".*\"/'d >> combine.c
done

for i in ${C[@]}; do
    echo "$i"
    sed '/\#include \".*\"/'d $i >> combine.c
done



