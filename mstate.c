#include <stdlib.h>
#include "mstate.h"
#include "mtable.h"

static void strtable_init(struct strtable* tab)
{
  tab->table = (MString**)calloc(sizeof(MString*),  MIN_TABLE_SIZE);
  tab->size = MIN_TABLE_SIZE;
  tab->total = 0;
}

void muaE_growstack(mua_State* M)
{
  if (M->top < M->stack_size) return;
  M->stack_size *= 2;
  M->stack = (MValue*)realloc(M->stack, sizeof(MValue) * M->stack_size);
}

mua_State* mua_newstate(void)
{
  mua_State* M = (mua_State*)malloc(sizeof(mua_State));
  M->stack = (MValue*)malloc(sizeof(MValue)*MIN_STACK_SIZE);
  M->save_base = (int*)malloc(sizeof(int)*MIN_STACK_SIZE);
  M->stack_size = MIN_STACK_SIZE;
  M->base_size = MIN_STACK_SIZE;
  M->top = 1;
  M->base = 0;
  M->call_depth = 0;
  M->G = (struct global_State*)malloc(sizeof(struct global_State));
  M->G->g_env = muaT_new(M);
  strtable_init(&M->G->strtab);

  return M;
}
