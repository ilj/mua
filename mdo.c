#include <stdlib.h>
#include "mdo.h"
#include "mstate.h"

static void enter(mua_State* M)
{
  if (M->call_depth >= M->base_size) {
    M->base_size *= 2;
    M->save_base = (int*)realloc(M->save_base, sizeof(int) * M->base_size);
  }

  M->save_base[M->call_depth++] = M->base;
}

static void leave(mua_State* M)
{
  M->top = M->base+1;
  M->base = M->save_base[--M->call_depth];
}

void muaD_call(mua_State *M, int total)
{
  MFunction* f;
  
  enter(M);
  M->base = M->top - total-1;
  mua_assert(objfunc(stackfuncvalue(M)));
  f = value2func(stackfuncvalue(M));

  switch (f->type) {
  case MUA_CFUNCTION:
    f->func.c_func(M);
    muaD_return(M);
    break;
  case MUA_MFUNCTION:
    muaV_execute(M);
    break;
  default:
    mua_assert(0);
  }
}

void muaD_return(mua_State* M)
{
  setstackvalue(M, M->base, stacktopvalue(M));
  leave(M);
}
