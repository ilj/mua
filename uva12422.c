#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

/*******************************************************************************/
//FOR INCLUDED

#define EOZ EOF

#define FIRST_RESERVED	257

enum RESERVED {
  TK_AND = FIRST_RESERVED, TK_BREAK,
  TK_DO, TK_ELSE, TK_ELSEIF, TK_END, TK_FALSE, TK_FOR, TK_FUNCTION,
  TK_GOTO, TK_IF, TK_IN, TK_LOCAL, TK_NIL, TK_NOT, TK_OR, TK_REPEAT,
  TK_RETURN, TK_THEN, TK_TRUE, TK_UNTIL, TK_WHILE,

  TK_CONCAT, TK_DOTS, TK_EQ, TK_GE, TK_LE, TK_NE, TK_DBCOLON, TK_EOS,
  TK_NUMBER, TK_NAME, TK_STRING, TK_EOL
};

#define NUM_RESERVED	(int)(TK_WHILE-FIRST_RESERVED+1)

#define MAX_BUFFERSIZE 64

#define save_and_next(ls)  (save(ls, ls->current), next(ls))

#define lislalpha(c)	(isalpha(c) || (c) == '_')
#define lislalnum(c)	(isalnum(c) || (c) == '_')
#define lisdigit(c)	(isdigit(c))
#define lisspace(c)	(isspace(c))
#define lisprint(c)	(isprint(c))
#define lisxdigit(c)	(isxdigit(c))

typedef struct Buffer {
  char *p;
  int size, len;
} Buffer;

#define mua_init_buffer(buff) ((buff)->p = (char*)calloc(MAX_BUFFERSIZE, 1), (buff)->size = MAX_BUFFERSIZE, (buff)->len = 0)
#define mua_clear_buffer(buff) ((buff)->len = (buff)->size)
#define mua_close_buffer(buff) (free((buff)->p), (buff)->p = NULL)
#define mua_extend_buffer(buff) ((buff)->size *= 2, (buff)->p = (char*)realloc((buff)->p, (buff)->size))

typedef int (*Mua_Reader)(Buffer* buff, void** data);

typedef struct MIO {
  Mua_Reader reader;
  Buffer* buff;
  void* data;
} MIO;

int MIO_stdin_reader(Buffer* buff, void** data);

typedef struct LexState {
  Buffer* buff;
  MIO* source;
  int current;
  int token;
} LexState;

void muaX_init(LexState* ls, MIO* io, Buffer* buff);
int muaX_next(LexState* ls);

/*****************************************************************************/

static const char *const muaX_tokens [] = {
    "and", "break", "do", "else", "elseif",
    "end", "false", "for", "function", "goto", "if",
    "in", "local", "nil", "not", "or", "repeat",
    "return", "then", "true", "until", "while",

    "..", "...", "==", ">=", "<=", "~=", "::", "<eof>",
    "<number>", "<name>", "<string>", "<eol>"
};

static int MIO_next(MIO *io)
{
  char *p = (char*)io->data;
  io->data = p+1;
  return *p;
}

static void lexerror(LexState *ls, const char* msg, int tk)
{
  printf("[lexerror]: ");
  if (tk < FIRST_RESERVED) 
    printf("%c:", tk); 
  else 
    printf("%s:", muaX_tokens[tk-FIRST_RESERVED]);
  printf("%s\n", msg);
}

static void next(LexState* ls) 
{
  if (ls->source->reader(ls->source->buff, &ls->source->data) == EOZ) {
    ls->current = EOZ;
    return;
  }
  ls->current = MIO_next(ls->source);
}

static void save(LexState* ls, int c)
{
  if (ls->buff->len >= ls->buff->size) {
    mua_extend_buffer(ls->buff);
  }
  ls->buff->p[ls->buff->len++] = c;
}

static int check_next (LexState *ls, const char *set) {
  if (ls->current == '\0' || !strchr(set, ls->current))
    return 0;
  save_and_next(ls);
  return 1;
}

static int check_reserved(Buffer* buff)
{
  int i;
  for (i = 0; i < NUM_RESERVED; ++i) {
    if (strcmp(buff->p, muaX_tokens[i]) == 0) return i + FIRST_RESERVED;
  }
  return -1;
}

static void read_numeral (LexState *ls) {
  const char *expo = "Ee";
  int first = ls->current;

  save_and_next(ls);
  if (first == '0' && check_next(ls, "Xx"))  /* hexadecimal? */
    expo = "Pp";
  for (;;) {
    if (check_next(ls, expo))  /* exponent part? */
      check_next(ls, "+-");  /* optional exponent sign */
    if (lisxdigit(ls->current) || ls->current == '.')
      save_and_next(ls);
    else
      break;
  }
  save(ls, '\0');
}

static void read_string (LexState *ls) {
  int del = ls->current;
  save_and_next(ls);  /* keep delimiter (for error messages) */
  while (ls->current != del) {
    switch (ls->current) {
      case EOZ:
        lexerror(ls, "unfinished string", TK_EOS);
        break;  /* to avoid warnings */
      case '\n':
      case '\r':
        lexerror(ls, "unfinished string", TK_STRING);
        break;  /* to avoid warnings */
      case '\\':
	save_and_next(ls);
	save_and_next(ls);
	break;
      default:
        save_and_next(ls);
    }
  }
  save_and_next(ls);  /* skip delimiter */
  save(ls, '\0');
}

static int lex(LexState* ls)
{
  ls->buff->len = 0;
  for (;;) {
    switch (ls->current) {
      case '\n' : case '\r' : {
	next(ls);
	return TK_EOL;
      }
      case ' ': case '\f': case '\t': case '\v': {  /* spaces */
        next(ls);
        break;
      }
      case '-': { 
        next(ls);
        if (ls->current != '-') return '-';
        next(ls);
        while (ls->current != '\n' && ls->current != '\r' && ls->current != EOZ)
          next(ls);
        break;
      }
      case '[': {
	next(ls);
        return '[';
      }
      case '=': {
        next(ls);
        if (ls->current != '=') return '=';
        else { next(ls); return TK_EQ; }
      }
      case '<': {
        next(ls);
        if (ls->current != '=') return '<';
        else { next(ls); return TK_LE; }
      }
      case '>': {
        next(ls);
        if (ls->current != '=') return '>';
        else { next(ls); return TK_GE; }
      }
      case '~': {
        next(ls);
        if (ls->current != '=') return '~';
        else { next(ls); return TK_NE; }
      }
      case ':': {
        next(ls);
        if (ls->current != ':') return ':';
        else { next(ls); return TK_DBCOLON; }
      }
      case '"': case '\'': {  /* short literal strings */
        read_string(ls);
        return TK_STRING;
      }
      case '.': {  /* '.', '..', '...', or number */
        save_and_next(ls);
        if (check_next(ls, ".")) {
          if (check_next(ls, "."))
            return TK_DOTS;   /* '...' */
          else return TK_CONCAT;   /* '..' */
        }
        else if (!lisdigit(ls->current)) return '.';
        /* else go through */
      }
      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9': {
        read_numeral(ls);
        return TK_NUMBER;
      }
      case EOZ: {
        return TK_EOS;
      }
      default: {
	int tk;
        if (lislalpha(ls->current)) {  /* identifier or reserved word? */
	  do {
            save_and_next(ls);
          } while (lislalnum(ls->current));
	  save(ls, '\0');
	  tk = check_reserved(ls->buff);
	  if (tk != -1) 
	    return tk;
	  else
	    return TK_NAME;
        }
        else {  /* single-char tokens (+ - / ...) */
          int c = ls->current;
          next(ls);
          return c;
        }
      }
    }
  }
}

void muaX_init(LexState* ls, MIO* s, Buffer* buff)
{
  ls->buff = buff;
  ls->source = s;
  next(ls);
}

int muaX_next(LexState* ls)
{
  return ls->token = lex(ls);
}

int MIO_stdin_reader(Buffer* buff, void** data)
{
  char* p = *(char**)data;
  if (p == NULL || p >= buff->p + buff->len) {
    if (NULL == fgets(buff->p, buff->size, stdin)) return EOZ;
    buff->len = strlen(buff->p);
    *data = buff->p;
  }
  
  return buff->len;
}

void parse_tokens()
{
  LexState ls;
  Buffer buff[2];
  MIO io;
  
  mua_init_buffer(&buff[0]);
  mua_clear_buffer(&buff[0]);
  mua_init_buffer(&buff[1]);
  io.data = NULL;
  io.buff = &buff[0];
  io.reader = MIO_stdin_reader;

  muaX_init(&ls, &io, &buff[1]);
  for (;;) {
    int tk;
    tk = muaX_next(&ls);
    if (tk == TK_EOS) break;

    if (tk == TK_EOL) {
      printf("[EOL]\n");
    } else if (tk == TK_NAME) {
      printf("[NAME] %s\n", ls.buff->p);
    } else if (FIRST_RESERVED <= tk && tk < FIRST_RESERVED+NUM_RESERVED) {
      printf("[RESERVED] %s\n", muaX_tokens[tk-FIRST_RESERVED]);
    } else if (tk == TK_STRING) {
      printf("[STRING] %s\n", ls.buff->p);
    } else if (tk == TK_NUMBER) {
      printf("[NUMBER] %s\n", ls.buff->p);
    } else {
      printf("[SYMBOL] ");
      if (tk < FIRST_RESERVED) {
	printf("%c\n", tk);
      } else {
	printf("%s\n", muaX_tokens[tk-FIRST_RESERVED]);
      }
    }
  }
}

int main()
{
  parse_tokens();
  return 0;
}

