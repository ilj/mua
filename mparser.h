#ifndef MPARSER_
#define MPARSER_

#include "mua.h"
#include "mlex.h"
#include "mtable.h"
#include "mutil.h"

enum {
  VT_CONST, //b -> k index
  VT_GLOBAL, // b -> k index
  VT_LOCAL, //b -> lv index
  VT_TABLE, //stacktop -> key stacktop-1 -> table
  VT_STACKTOP, // stacktop -> value a -> 1 (1 value calculated)
};

typedef struct ExpInfo {
  int type; // left value location
  int a, b;
  int ni, li;
} ExpInfo;

typedef struct BlockInfo {
  int last_lv_idx;
  struct BlockInfo* prev;
} BlockInfo;

typedef struct LocalVarInfo {
  MString* name;
  int lv_idx;
} LocalVarInfo;

typedef struct ParseState {
  mua_State* M;
  LocalVarInfo* lv;
  int lv_size, lv_len;
  MTable* k_env;
  MProto* p;
  LexState* ls;
  BlockInfo* blk;
} ParseState;

void muaP_parse_simple_code(ParseState* ps);

#endif
