#ifndef MSTATE_
#define MSTATE_

#include "mobject.h"

#define MIN_STACK_SIZE 65536
#define MIN_TABLE_SIZE 32

struct strtable {
  MString** table;
  int size;
  int total;
};

struct global_State {
  MTable* g_env;
  struct strtable strtab;
};

struct mua_State {
  struct global_State* G;
  MValue* stack;
  int* save_base;
  int call_depth;
  int base, top;
  int stack_size;
  int base_size;
};

#define pushnil(M) (setnil((M)->stack + ((M)->top++)))
#define pushvalue(M, v) ((M)->stack[(M)->top++] = (*(v)))
#define setstackvalue(M, idx, v) ((M)->stack[(idx)] = (*(v)))

#define stacktopvalue(M) ((M)->stack + (M)->top-1)
#define stackidxvalue(M, idx) ((M)->stack + (idx))
#define stackfuncvalue(M) ((M)->stack + (M)->base)
#define stackargvalue(M, idx) ((M)->stack + (M)->base + (idx))

#define inctop(M) ++((M)->top)
#define dectop(M, size) ((M)->top -= size)

#define getfuncproto(M, base) (value2func((M)->stack + (base))->func.m_func)

void muaE_growstack(mua_State* M);

#endif
