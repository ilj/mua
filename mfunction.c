#include <stdlib.h>
#include "mfunction.h"

#define MIN_VALUE_ARRAY_SIZE 1

MFunction* muaF_newC(mua_State* M, Mua_CFunction func)
{
  MFunction* f = (MFunction*)muaB_newobj(M, MUA_CFUNCTION, sizeof(*f));
  f->func.c_func = func;
  return f;
}

MFunction* muaF_newM(mua_State* M)
{
  MFunction* f = (MFunction*)muaB_newobj(M, MUA_MFUNCTION, sizeof(*f));
  f->func.m_func = (MProto*)malloc(sizeof(MProto));
  f->func.m_func->k = (MValue*)malloc(sizeof(MValue) * MIN_VALUE_ARRAY_SIZE);
  f->func.m_func->k_size = MIN_VALUE_ARRAY_SIZE;
  f->func.m_func->k_len = 0;
  f->func.m_func->lv_len = 0;
  f->func.m_func->code = (Instruction*)malloc(sizeof(Instruction) * MIN_VALUE_ARRAY_SIZE);
  f->func.m_func->code_size = MIN_VALUE_ARRAY_SIZE;
  f->func.m_func->code_len = 0;

  return f;
}

#ifdef TEST_MFUNCTION
int main()
{
}
#endif
