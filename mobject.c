#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "mobject.h"
#include "mstring.h"
#include "mutil.h"

#define mua_numequ(a,b) (fabs((a)-(b)) < 1e-9)

MObject* muaB_newobj(mua_State* M, int type, int size)
{
  MObject* result = NULL;
  result = (MObject*)malloc(size);
  result->type = type;
  result->next = NULL;
  return result;
}

int muaB_checkequ(mua_State* M, MValue* a, MValue* b)
{
  if (a->type != b->type) return 0;
  switch (a->type) {
  case MUA_NIL:
    return 1;
  case MUA_BOOL:
    return value2bool(a) == value2bool(b);
  case MUA_NUMBER: 
    return mua_numequ(value2num(a), value2num(b));
  default:
    return value2obj(a) == value2obj(b);
  }
}

union DoubleHash
{
  char b[4];
  double v;
};

union PointerHash
{
  char b[4];
  void* v;
};

int muaB_gethash(mua_State* M, MValue* o)
{
  int i, h;
  union DoubleHash dh;
  union PointerHash ph;

  switch (o->type) {
  case MUA_NIL:
    return 0;
  case MUA_NUMBER: 
    dh.v = value2num(o);
    h = 0;
    for (i = 3; i >= 0; --i) {
      h = h ^ ((h << 5) + (h >> 2) + dh.b[i]);
    }
    return h;
  case MUA_STRING:
    return value2str(o)->hash;
  default:
    ph.v = value2obj(o);
    h = 0;
    for (i = 3; i >= 0; --i) {
      h = h ^ ((h << 5) + (h >> 2) + ph.b[i]);
    }
    return h;
  }
}

void muaB_unary_arith(MValue* o, MValue* result, int op)
{
  switch (op) {
  case MUA_NOT:
    setboolvalue(result, !value2bool(o));
    break;
  case MUA_NEG:
    setnumvalue(result, -value2num(o));
    break;
  default:
    mua_assert(0);
  }
}

void muaB_bool_arith(MValue* o, MValue* p, MValue* result, int op)
{
  switch (op) {
  case MUA_LESS:
    setboolvalue(result, value2num(o) < value2num(p));
    break;
  case MUA_GREATER:
    setboolvalue(result, value2num(o) > value2num(p));
    break;
  case MUA_LESSEQU:
    setboolvalue(result, value2num(o) <= value2num(p));
    break;
  case MUA_GREATEREQU:
    setboolvalue(result, value2num(o) >= value2num(p));
    break;
  case MUA_NOTEQU:
    setboolvalue(result, !mua_numequ(value2num(o), value2num(p)));
    break;
  case MUA_EQU:
    setboolvalue(result, mua_numequ(value2num(o), value2num(p)));
    break;
  default:
    mua_assert(0);
  }
}

void muaB_num_arith(MValue* o, MValue* p, MValue* result, int op)
{
  MuaInteger a, b;
  double x = value2num(o);
  double y = value2num(p);
  switch (op) {
  case MUA_ADD:
    setnumvalue(result, x + y);
    break;
  case MUA_MINUS:
    setnumvalue(result, x - y);
    break;
  case MUA_MULT:
    setnumvalue(result, x * y);
    break;
  case MUA_DIV:
    setnumvalue(result, x / y);
    break;
  case MUA_MOD:
    a = (MuaInteger)x;
    b = (MuaInteger)y;
    setnumvalue(result, a%b);
    break;
  case MUA_POW:
    setnumvalue(result, pow(x, y));
    break;
  default:
    mua_assert(0);
  }
}

void muaB_str2d(const char* s, int len, double *n)
{
  *n = strtod(s, NULL);
}

#ifdef TEST_MOBJECT
int main()
{
}
#endif
