#include <stdio.h>
#include "mua.h"

int pass(mua_State* M)
{
  mua_pushnumber(M, 1);
  return 1;
}

int display_pass(mua_State* M)
{
  int top = mua_gettop(M);
  double n = mua_tonumber(M, top);
  printf("%.0lf\n", n);
  return 1;
}

int main()
{
  mua_State* M = mua_newstate();
  mua_pushcfunction(M, display_pass);
  mua_pushcfunction(M, pass);
  mua_call(M, 0);
  mua_call(M, 1);
  mua_pushcfunction(M, display_pass);
  mua_pushvalue(M, mua_gettop(M)-1);
  mua_call(M, 1);
  
  return 0;
}
