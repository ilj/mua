#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mutil.h"
#include "mlex.h"

static const char *const muaX_tokens [] = {
    "and", "break", "do", "else", "elseif",
    "end", "false", "for", "function", "goto", "if",
    "in", "local", "nil", "not", "or", "repeat",
    "return", "then", "true", "until", "while",

    "..", "...", "==", ">=", "<=", "~=", "::", "<eof>",
    "<number>", "<name>", "<string>", "<eol>"
};

static int MIO_next(MIO *io)
{
  char *p = (char*)io->data;
  io->data = p+1;
  return *p;
}

static void lexerror(LexState *ls, const char* msg, int tk)
{
  printf("[lexerror]: ");
  if (tk < FIRST_RESERVED) 
    printf("%c:", tk); 
  else 
    printf("%s:", muaX_tokens[tk-FIRST_RESERVED]);
  printf("%s\n", msg);
}

static void next(LexState* ls) 
{
  if (ls->source->reader(ls->source->buff, &ls->source->data) == EOZ) {
    ls->current = EOZ;
    return;
  }
  ls->current = MIO_next(ls->source);
}

static void save(LexState* ls, int c)
{
  if (ls->buff->len >= ls->buff->size) {
    mua_extend_buffer(ls->buff);
  }
  ls->buff->p[ls->buff->len++] = c;
}

static int check_next (LexState *ls, const char *set) {
  if (ls->current == '\0' || !strchr(set, ls->current))
    return 0;
  save_and_next(ls);
  return 1;
}

static int check_reserved(Buffer* buff)
{
  int i;
  for (i = 0; i < NUM_RESERVED; ++i) {
    if (strcmp(buff->p, muaX_tokens[i]) == 0) return i + FIRST_RESERVED;
  }
  return -1;
}

static void read_numeral (LexState *ls) {
  const char *expo = "Ee";
  int first = ls->current;

  save_and_next(ls);
  if (first == '0' && check_next(ls, "Xx"))  /* hexadecimal? */
    expo = "Pp";
  for (;;) {
    if (check_next(ls, expo))  /* exponent part? */
      check_next(ls, "+-");  /* optional exponent sign */
    if (lisxdigit(ls->current) || ls->current == '.')
      save_and_next(ls);
    else
      break;
  }
  save(ls, '\0');
}

static void read_string (LexState *ls) {
  int del = ls->current;
  save_and_next(ls);  /* keep delimiter (for error messages) */
  while (ls->current != del) {
    switch (ls->current) {
      case EOZ:
        lexerror(ls, "unfinished string", TK_EOS);
        break;  /* to avoid warnings */
      case '\n':
      case '\r':
        lexerror(ls, "unfinished string", TK_STRING);
        break;  /* to avoid warnings */
      case '\\':
	save_and_next(ls);
	save_and_next(ls);
	break;
      default:
        save_and_next(ls);
    }
  }
  save_and_next(ls);  /* skip delimiter */
  save(ls, '\0');
}

static int lex(LexState* ls)
{
  ls->buff->len = 0;
  for (;;) {
    switch (ls->current) {
      case '\n' : case '\r' : {
	ls->line_cnt += 1;
	next(ls);
	//	return TK_EOL;
	break;
      }
      case ' ': case '\f': case '\t': case '\v': {  /* spaces */
        next(ls);
        break;
      }
      case '-': { 
        next(ls);
        if (ls->current != '-') return '-';
        next(ls);
        while (ls->current != '\n' && ls->current != '\r' && ls->current != EOZ)
          next(ls);
	ls->line_cnt += 1;
        break;
      }
      case '[': {
	next(ls);
        return '[';
      }
      case '=': {
        next(ls);
        if (ls->current != '=') return '=';
        else { next(ls); return TK_EQ; }
      }
      case '<': {
        next(ls);
        if (ls->current != '=') return '<';
        else { next(ls); return TK_LE; }
      }
      case '>': {
        next(ls);
        if (ls->current != '=') return '>';
        else { next(ls); return TK_GE; }
      }
      case '~': {
        next(ls);
        if (ls->current != '=') return '~';
        else { next(ls); return TK_NE; }
      }
      case ':': {
        next(ls);
        if (ls->current != ':') return ':';
        else { next(ls); return TK_DBCOLON; }
      }
      case '"': case '\'': {  /* short literal strings */
        read_string(ls);
        return TK_STRING;
      }
      case '.': {  /* '.', '..', '...', or number */
        save_and_next(ls);
        if (check_next(ls, ".")) {
          if (check_next(ls, "."))
            return TK_DOTS;   /* '...' */
          else return TK_CONCAT;   /* '..' */
        }
        else if (!lisdigit(ls->current)) return '.';
        /* else go through */
      }
      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9': {
        read_numeral(ls);
        return TK_NUMBER;
      }
      case EOZ: {
	save(ls, 'E'); save(ls,'O'); save(ls, 'Z'); save(ls, '\0');
        return TK_EOS;
      }
      default: {
	int tk;
        if (lislalpha(ls->current)) {  /* identifier or reserved word? */
	  do {
            save_and_next(ls);
          } while (lislalnum(ls->current));
	  save(ls, '\0');
	  tk = check_reserved(ls->buff);
	  if (tk != -1) 
	    return tk;
	  else
	    return TK_NAME;
        }
        else {  /* single-char tokens (+ - / ...) */
          int c = ls->current;
          next(ls);
          return c;
        }
      }
    }
  }
}

void muaX_init(LexState* ls, MIO* s, Buffer* buff)
{
  ls->buff = buff;
  ls->source = s;
  ls->line_cnt = 0;
  next(ls);
}

int muaX_next(LexState* ls)
{
  return ls->token = lex(ls);
}

void muaX_trip_str(LexState* ls)
{
  int la, i;
  int len;
  char c;
  
  if (ls->buff->len == 0 || (ls->buff->p[0] != '\'' && ls->buff->p[0] != '"')) return;
  len = ls->buff->len-2;
  
  for (la = 0, i = 1; i < len; ++i) {
    if (ls->buff->p[i] == '\\') {
      ++i;
      switch (ls->buff->p[i]) {
      case 'a': c = '\a'; goto trip_save;
      case 'b': c = '\b'; goto trip_save;
      case 'f': c = '\f'; goto trip_save;
      case 'n': c = '\n'; goto trip_save;
      case 'r': c = '\r'; goto trip_save;
      case 't': c = '\t'; goto trip_save;
      case 'v': c = '\v'; goto trip_save;
      case '\n': case '\r':
	ls->line_cnt++; 
	c = '\n'; goto trip_save;
      default:
	c = ls->buff->p[i]; goto trip_save;
      }
    } else {
      c = ls->buff->p[i];
    }

  trip_save:
    ls->buff->p[la++] = c;
  }

  ls->buff->p[la++] = '\0';
}

#ifdef TEST_LEX

void parse_tokens()
{
  LexState ls;
  Buffer buff[2];
  MIO io;
  
  mua_init_buffer(&buff[0]);
  mua_clear_buffer(&buff[0]);
  mua_init_buffer(&buff[1]);
  io.data = NULL;
  io.buff = &buff[0];
  io.reader = MIO_stdin_reader;

  muaX_init(&ls, &io, &buff[1]);
  for (;;) {
    int tk;
    tk = muaX_next(&ls);
    if (tk == TK_EOS) break;

    if (tk == TK_EOL) {
      printf("[EOL]\n");
    } else if (tk == TK_NAME) {
      printf("[NAME] %s\n", ls.buff->p);
    } else if (FIRST_RESERVED <= tk && tk < FIRST_RESERVED+NUM_RESERVED) {
      printf("[RESERVED] %s\n", muaX_tokens[tk-FIRST_RESERVED]);
    } else if (tk == TK_STRING) {
      printf("[STRING] %s\n", ls.buff->p);
    } else if (tk == TK_NUMBER) {
      printf("[NUMBER] %s\n", ls.buff->p);
    } else {
      printf("[SYMBOL] ");
      if (tk < FIRST_RESERVED) {
	printf("%c\n", tk);
      } else {
	printf("%s\n", muaX_tokens[tk-FIRST_RESERVED]);
      }
    }
  }
}

int main()
{
  parse_tokens();
  return 0;
}

#endif
