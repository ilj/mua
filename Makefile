MYMARCO=
INC=mstate.h mobject.h mstring.h mtable.h mfunction.h mlex.h mutil.h mvm.h mdo.h mapi.h mcode.h mparser.h mopcode.h mbaselib.h
OBJ=mstate.o mobject.o mstring.o mtable.o mfunction.o mlex.o mutil.o mvm.o mdo.o mapi.o mcode.o mparser.o mopcode.o mbaselib.o
CFLAGS=-lm -g
TEST_OBJ=test_cfunc.o test_parser.o
TEST_EXEC=test_cfunc test_parser

ALLOBJ=$(OBJ) $(TEST_OBJ)

%.o: CFLAGS=-lm -g

all:	$(OBJ) test

test:	$(ALLOBJ)
	cc -o test_cfunc $(MYMARCO) $(CFLAGS) $(OBJ) test_cfunc.o 
	cc -o test_parser $(MYMARCO) $(CFLAGS) $(OBJ) test_parser.o 

clean:
	rm $(ALLOBJ) $(TEST_EXEC)