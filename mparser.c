#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mstring.h"
#include "mopcode.h"
#include "mcode.h"
#include "mparser.h"

#define ps_tk (ps->ls->token)

#define parse_error(ln, errstr) { fprintf(stderr, "[error line:%d]: %s", (ln), (errstr)); mua_assert(0); }

static const struct {
  int left;  /* left priority for each binary operator */
  int right; /* right priority */
} priority[] = {  /* ORDER OPR */
   {6, 6}, {6, 6}, {7, 7}, {7, 7}, {7, 7},  /* `+' `-' `*' `/' `%' */
   {10, 9}, {5, 4},                 /* ^, .. (right associative) */
   {3, 3}, {3, 3}, {3, 3},          /* ==, <, <= */
   {3, 3}, {3, 3}, {3, 3},          /* ~=, >, >= */
   {2, 2}, {1, 1}                   /* and, or */
};

#define UNARY_PRIORITY	8  /* priority for unary operators */

static UnOpr get_unopr (int op) {
  switch (op) {
    case TK_NOT: return OPR_NOT;
    case '-': return OPR_MINUS;
    case '#': return OPR_LEN;
    default: return OPR_NOUNOPR;
  }
}

static BinOpr get_binopr (int op) {
  switch (op) {
    case '+': return OPR_ADD;
    case '-': return OPR_SUB;
    case '*': return OPR_MUL;
    case '/': return OPR_DIV;
    case '%': return OPR_MOD;
    case '^': return OPR_POW;
    case TK_CONCAT: return OPR_CONCAT;
    case TK_NE: return OPR_NE;
    case TK_EQ: return OPR_EQ;
    case '<': return OPR_LT;
    case TK_LE: return OPR_LE;
    case '>': return OPR_GT;
    case TK_GE: return OPR_GE;
    case TK_AND: return OPR_AND;
    case TK_OR: return OPR_OR;
    default: return OPR_NOBINOPR;
  }
}

static void add_left_var_code(ParseState* ps, ExpInfo* ei)
{
  switch (ei->type) {
  case  VT_LOCAL:
    muaC_addcode(ps->p, OP_SETLOCAL, 0, ei->b);
    break;
  case VT_GLOBAL:
    muaC_addcode(ps->p, OP_SETGLOBAL, 0, ei->b);
    break;
  case VT_TABLE:
    muaC_addcode(ps->p, OP_SETTABLE, 0, 0);
    break;
  default:
    parse_error(ps->ls->line_cnt, "var doesn\'t has left value");
  }
}

static void add_right_var_code(ParseState* ps, ExpInfo* ei)
{
  switch (ei->type) {
  case  VT_LOCAL:
    muaC_addcode(ps->p, OP_PUSHLOCAL, 0, ei->b);
    break;
  case VT_GLOBAL:
    muaC_addcode(ps->p, OP_PUSHGLOBAL, 0, ei->b);
    break;
  case VT_TABLE:
    muaC_addcode(ps->p, OP_GETTABLE, 0, 0);
    break;
  default:
    parse_error(ps->ls->line_cnt, "var doesn\'t has right value");
  }
}


static int find_k_idx(ParseState* ps, MValue* v)
{
  int idx;
  MValue* pv;
  MValue k;
  pv = muaT_get(ps->M, ps->k_env, v);
  if (pv == NULL) {
    idx = muaC_addk(ps->p, v);
    setnumvalue(&k, idx);
    muaT_set(ps->M, ps->k_env, v, &k);
  } else {
    idx = value2integer(pv);
  }
  return idx;
}

static void enterblock(ParseState* ps, BlockInfo* b)
{
  b->last_lv_idx = ps->lv_len;
  b->prev = ps->blk;
  ps->blk = b;
}

static void leaveblock(ParseState* ps)
{
  if (ps->blk) {
    ps->lv_len = ps->blk->last_lv_idx;
    ps->blk = ps->blk->prev;
  }
}

static int searchlocalvar(ParseState* ps, MString* name)
{
  int i;
  for (i = ps->lv_len-1; i >= 0; --i) {
    if (equstr(name, ps->lv[i].name)) {
      return i;
    }
  }
  return -1;
}

static int newlocalvar(ParseState* ps, MString* name)
{
  if (ps->lv_len >= ps->lv_size) {
    ps->lv_size *= 2;
    ps->lv = (LocalVarInfo*)realloc(ps->lv, sizeof(*ps->lv) * ps->lv_size);
  }
  ps->lv[ps->lv_len].name = name;
  ps->p->lv_len++;
  return ps->lv_len++;
}

static void parse_simpleexpr(ParseState* ps, ExpInfo *ei);
static BinOpr parse_subexpr(ParseState* ps, ExpInfo* ei, int pri);
static void parse_expr(ParseState* ps, ExpInfo* ei);
static void parse_var(ParseState* ps, ExpInfo* ei);
static void parse_function_call(ParseState* ps, ExpInfo* ei);
static int parse_statement(ParseState* ps, ExpInfo* ei);
static void parse_simplestatment(ParseState* ps, ExpInfo* ei);
static void parse_ifstatement(ParseState* ps, ExpInfo* ei);
static void parse_block(ParseState* ps, ExpInfo* ei);
static void parse_whilestatement(ParseState* ps, ExpInfo* ei);
static void parse_repeatstatement(ParseState* ps, ExpInfo* ei);

void parse_simpleexpr(ParseState* ps, ExpInfo *ei)
{
  double n;
  ExpInfo el, er;
  MString* s;
  MValue v;
  int idx;
  switch (ps_tk) {
  case TK_NUMBER: 
    muaB_str2d(token_str(ps->ls), strlen(token_str(ps->ls)), &n);
    setnumvalue(&v, n);
    goto NEW_CONST;
  case TK_STRING:
    muaX_trip_str(ps->ls);
    s = muaS_newstr(ps->M, token_str(ps->ls));
    setstrvalue(&v, s);
    goto NEW_CONST;
  case TK_NIL:
    setnil(&v);
    goto NEW_CONST;
  case TK_FALSE:
    setboolvalue(&v, 0);
    goto NEW_CONST;
  case TK_TRUE:
    setboolvalue(&v, 1);
    goto NEW_CONST;
  case '{':
    muaX_next(ps->ls);
    if (ps_tk != '}') parse_error(ps->ls->line_cnt, "expected \'}\' token");
    muaX_next(ps->ls);
    muaC_addcode(ps->p, OP_NEWTABLE, 0, 0);
    return;
  case TK_NAME:
    parse_var(ps, &el);
    add_right_var_code(ps, &el);
    if (ps_tk == '(') {
      parse_function_call(ps, &er);
    }
    return;
  default:
    parse_error(ps->ls->line_cnt, "not a simpleexpr");
  }

 NEW_CONST:
  idx = find_k_idx(ps, &v);
  ei->type = VT_CONST;
  ei->a = 1;
  ei->b = idx;
  muaC_addcode(ps->p, OP_PUSHCONST, 0, idx);
  muaX_next(ps->ls);
}

BinOpr parse_subexpr(ParseState* ps, ExpInfo* ei, int current_pri)
{
  BinOpr binop, nextop;
  UnOpr unop;
  ExpInfo er, el;
  
  unop = get_unopr(ps_tk);
  if (ps_tk == '(') {
    muaX_next(ps->ls);
    parse_expr(ps, &er);
    if (ps_tk != ')') parse_error(ps->ls->line_cnt, "expected \')\' token");
    muaX_next(ps->ls);
  } else if (unop != OPR_NOUNOPR) {
    muaX_next(ps->ls);
    parse_subexpr(ps, &er, UNARY_PRIORITY);
    muaC_addunop(ps->p, unop);
  } else {
    parse_simpleexpr(ps, &el);
  }

  binop = get_binopr(ps_tk);
  while (binop != OPR_NOBINOPR && priority[binop].left > current_pri) {
    muaX_next(ps->ls);
    nextop = parse_subexpr(ps, &er, priority[binop].right);
    muaC_addbinop(ps->p, binop);
    binop = nextop;
  }

  return binop;
}


void parse_expr(ParseState* ps, ExpInfo *ei)
{
  parse_subexpr(ps, ei, 0);
  ei->a = 1;
  ei->li = ps->p->code_len;
}

void parse_var(ParseState* ps, ExpInfo *ei)
{
  int idx;
  MValue v;
  ExpInfo ef;
  MString* s;

  s = muaS_newstr(ps->M, token_str(ps->ls));
  idx = searchlocalvar(ps, s);
  setstrvalue(&v, s);
  muaX_next(ps->ls);

  if (ps_tk != '.' && ps_tk != '[') {
    ei->type = 0 <= idx ? VT_LOCAL : VT_GLOBAL;
    ei->b = 0 <= idx ? idx : find_k_idx(ps, &v);
    return;
  } else {
    muaC_addcode(ps->p, 0 <= idx ? OP_PUSHLOCAL : OP_PUSHGLOBAL, 0, 0 <= idx ? idx : find_k_idx(ps, &v));
    
    if (ps_tk == '.') {
      muaX_next(ps->ls);
      if (ps_tk != TK_NAME) parse_error(ps->ls->line_cnt, "expected name token");
      s = muaS_newstr(ps->M, token_str(ps->ls));
      setstrvalue(&v, s);
      idx = find_k_idx(ps, &v);
      muaC_addcode(ps->p, OP_PUSHCONST, 0, idx);
    } else if (ps_tk == '[') {
      muaX_next(ps->ls);
      parse_expr(ps, &ef);
      if (ef.a != 1) parse_error(ps->ls->line_cnt, "expected invalid index");
      if (ps_tk != ']') parse_error(ps->ls->line_cnt, "expected \']\' token");
    }
    muaX_next(ps->ls);
  }

  for (;;) {
    if (ps_tk == '.') {
      muaC_addcode(ps->p, OP_GETTABLE, 0, 0);
      muaX_next(ps->ls);
      if (ps_tk != TK_NAME) parse_error(ps->ls->line_cnt, "expected name token");
      s = muaS_newstr(ps->M, token_str(ps->ls));
      setstrvalue(&v, s);
      idx = find_k_idx(ps, &v);
      muaC_addcode(ps->p, OP_PUSHCONST, 0, idx);
      muaX_next(ps->ls);
    } else if (ps_tk == '[') {
      muaC_addcode(ps->p, OP_GETTABLE, 0, 0);
      muaX_next(ps->ls);
      parse_expr(ps, &ef);
      if (ef.a != 1) parse_error(ps->ls->line_cnt, "expected invalid index");
      if (ps_tk != ']') parse_error(ps->ls->line_cnt, "expected \']\' token");
      muaX_next(ps->ls);
    } else {
      ei->type = VT_TABLE;
      break;
    }
  }
}

void parse_function_call(ParseState* ps, ExpInfo *ei)
{
  int args_total = 0;
  ExpInfo el;

  muaX_next(ps->ls);
  for (;;) {
    if (ps_tk != ')') {
      parse_expr(ps, &el);
      args_total += el.a;
      if (ps_tk != ')' && ps_tk != ',') parse_error(ps->ls->line_cnt, "expected \',\' or \')\' token");
    }
    if (ps_tk == ')') {
      muaC_addcode(ps->p, OP_CALL, args_total, 0);
      break;
    } else if (ps_tk == ',') {
      muaX_next(ps->ls);
    }
  }
  muaX_next(ps->ls);
}

void parse_simplestatment(ParseState* ps, ExpInfo* ei)
{
  ExpInfo el, er;
  parse_var(ps, &el);
  if (ps_tk == '(') {
    add_right_var_code(ps, &el);
    parse_function_call(ps, &er);
    muaC_addcode(ps->p, OP_POP, 1, 0);
  } else if (ps_tk == '=') {
    muaX_next(ps->ls);
    parse_expr(ps, &er);
    add_left_var_code(ps, &el);
  } else {
    parse_error(ps->ls->line_cnt, "expected \'(\' or '=' token");
  }
}

void parse_ifstatement(ParseState* ps, ExpInfo* ei)
{
  int ni, li;
  int fi;
  ExpInfo el, er;

  BlockInfo b;

  switch (ps_tk) {
  case TK_IF:
  case TK_ELSEIF:
    muaX_next(ps->ls);
    fi = ps->p->code_len;
    parse_expr(ps, &el);
    ni = muaC_addcode(ps->p, OP_JMP, 0, 0);
    if (ps_tk != TK_THEN) parse_error(ps->ls->line_cnt, "expected then token");
    muaX_next(ps->ls);
    enterblock(ps, &b);
    parse_block(ps, &er);
    leaveblock(ps);
    li = muaC_addcode(ps->p, OP_JMP, 1, 0);
    parse_ifstatement(ps, ei);
    muaC_setB(ps->p, ni, ei->ni);
    muaC_setB(ps->p, li, ei->li);
    ei->ni = fi;
    break;

  case TK_ELSE:
    muaX_next(ps->ls);
    fi = ps->p->code_len;
    enterblock(ps, &b);
    parse_block(ps, &er);
    leaveblock(ps);
    li = muaC_addcode(ps->p, OP_JMP, 1, 0);
    parse_ifstatement(ps, ei);
    muaC_setB(ps->p, li, ei->li);
    ei->ni = fi;
    break;
    
  case TK_END:
    muaX_next(ps->ls);
    ei->ni = ps->p->code_len;
    ei->li = ps->p->code_len;
    break;

  default:
    mua_assert(0);
  }
}

void parse_whilestatement(ParseState* ps, ExpInfo* ei)
{
  int si, ni;
  ExpInfo el;

  BlockInfo b;
  enterblock(ps, &b);
  
  muaX_next(ps->ls);
  si = ps->p->code_len;
  parse_expr(ps, &el);
  if (ps_tk != TK_DO) parse_error(ps->ls->line_cnt, "expected do token");
  muaX_next(ps->ls);
  ni = muaC_addcode(ps->p, OP_JMP, 0, 0);
  parse_block(ps, ei);
  if (ps_tk != TK_END) parse_error(ps->ls->line_cnt, "expected end token");
  muaX_next(ps->ls);
  muaC_addcode(ps->p, OP_JMP, 1, si);
  muaC_setB(ps->p, ni, ei->li+1);

  leaveblock(ps);
}

void parse_repeatstatement(ParseState* ps, ExpInfo* ei)
{
  int si;
  ExpInfo el;

  BlockInfo b;
  enterblock(ps, &b);

  muaX_next(ps->ls);
  si = ps->p->code_len;
  parse_block(ps, &el);
  if (ps_tk != TK_UNTIL) parse_error(ps->ls->line_cnt, "expected until token");
  muaX_next(ps->ls);
  parse_expr(ps, &el);
  muaC_addcode(ps->p, OP_JMP, 0, si);

  leaveblock(ps);
}

void parse_localvar(ParseState* ps, ExpInfo* ei)
{
  int idx;
  ExpInfo el;
  MString* s;

  muaX_next(ps->ls);
  mua_assert(ps_tk == TK_NAME);
  s = muaS_newstr(ps->M, token_str(ps->ls));
  idx = newlocalvar(ps, s);

  muaX_next(ps->ls);
  if (ps_tk == '=') {
    muaX_next(ps->ls);
    parse_expr(ps, &el);
    muaC_addcode(ps->p, OP_SETLOCAL, 0, idx);
  }
}

int parse_statement(ParseState* ps, ExpInfo* ei)
{
  switch (ps_tk) {
  case TK_EOS: 
    return TK_EOS;

  case TK_EOL:
    muaX_next(ps->ls);
    return TK_EOL;

  case TK_NAME:
    parse_simplestatment(ps, ei);
    return TK_NAME;
    
  case TK_IF:
    parse_ifstatement(ps, ei);
    return TK_IF;

  case TK_WHILE:
    parse_whilestatement(ps, ei);
    return TK_WHILE;
    
  case TK_REPEAT:
    parse_repeatstatement(ps, ei);
    return TK_REPEAT;

  case TK_LOCAL:
    parse_localvar(ps, ei);
    return TK_LOCAL;

  default:
    return TK_EOS;
  }
}

void parse_block(ParseState* ps, ExpInfo* ei)
{
  ExpInfo el;

  ei->ni = ps->p->code_len;
  for (;;) {
    if (parse_statement(ps, &el) == TK_EOS) break;
  }
  ei->li = ps->p->code_len;
}

void muaP_parse_simple_code(ParseState* ps)
{
  ExpInfo ei;

  muaX_next(ps->ls);
  parse_block(ps, &ei);
}
